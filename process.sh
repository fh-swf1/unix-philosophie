
: '
The script analyzes an Apache Logfile, which was found at http://www.almhuette-raith.at/apache-log/access.log

This is an example of the log: 

73.169.232.206 - - [16/Jan/2022:09:25:25 +0100] "GET /educationam HTTP/1.1" 404 217 "-" "Fuzz Faster U Fool v1.3.1-dev" "-"

The default format which is used by apache is defined by the following directive:

%h %l %u %t \"%r\" %>s %b

with:

%h      = IP-Adress of the client
%l      = Identity of the clientd determined by identd on the clients machine. Default is - when the information is not available
%u      = This userid of the client if the user was authenticated
%t      = The time when the request was recieved
\"%r\"  = The information about the request which includes the HTTP-Method, the requested resource path and the HTTP protocol
%>s     = The response of the server
%b      = The size of the requested object

There are some additional information which will not be recognized for the example.

'

cat $1 | 
  awk '{print $7}' | 
  sort             | 
  uniq -c          | 
  sort -r -n       | 
  head -n 5

: '
Sources:
    - https://www.keycdn.com/support/apache-access-log
    - http://www.almhuette-raith.at/apache-log/access.log
    - https://httpd.apache.org/docs/2.4/logs.html
'